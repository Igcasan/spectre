package com.mulejava.utils;

import com.mulejava.encryption.HMAC;

public class FacebookUtil {
	
	public static String generateHMAC(String key, String carrier_id, String algorithm) {
		String text = DateUtils.getTimestamp() + "" + carrier_id;
		return HMAC.digest(text,key,algorithm);
	}

}
