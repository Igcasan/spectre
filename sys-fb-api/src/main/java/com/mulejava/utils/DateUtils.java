package com.mulejava.utils;

import java.util.Date;

public class DateUtils {
    public static int getTimestamp() {
        return Math.round(new Date().getTime() / 1000);
    }
}